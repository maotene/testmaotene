# Título del Proyecto

Test de Mauricio Tene
## Tabla de Contenidos

1. [Instalación](#instalación)
2. [Uso](#uso)
3. [Rutas creadas](#ejemplos-de-código)


## Instalación

npm i o en su defecto npm i --force


## Uso

Se han creado dos rutas:

localhost:4200/test/test

Esta ruta es para la primera parte de la prueba


localhost:4200/test2/test2

es para la segunda parte de la prueba en donde se ha configurado material y se ha hecho lo que describe el documento:


Problema Práctico

La empresa Zurco Designio tiene la necesidad de desarrollar una página web que permita a uno de
sus principales clientes dar difusión a sus últimos libros.

Para esta prueba, no se cuenta con un API, por lo que será necesario simular el consumo de
información mediante mocks. El aspirante es libre de proponer la estructura y el contenido de los
mismos, así como su propuesta de diseño de la página en cuestión.

Algunos de los requerimientos proporcionados por el cliente son:

-La página debe mostrar al menos 3 de los libros más vendidos.
-Debe incluir una gráfica que permita visualizar cuál es el libro más vendido.
-Debe ser posible consultar información adicional sobre el libro a través de un mat-dialog o una
ventana modal.
-Una tabla que enumere al menos 10 de sus libros con datos como:
o Título
o Autor
o Fecha de publicación
o Número de páginas
o ISBN
o Stock
Extras:
-La tabla debe contar con un buscador para filtrar la información.
-La tabla debe incluir un paginador.

Se tomará en cuenta:
• La estructura proporcionada al proyecto.
• El uso de interfaces en la información.
• La personalización y detalle de los estilos del proyecto.
• Que tu diseño sea claro e intuitivo.
• La comunicación entre componentes.
• El uso y personalización de Angular Material.
• La implementación de Google Fonts.
• El uso de iconos.
• Diseño responsivo.

Recuerda
Siéntete libre de proponer una solución que demuestre tu potencial como desarrollador frontend.
Céntrate en la calidad de tu desarrollo; algunos componentes bien desarrollados serán mejor que
una gran cantidad sin calidad. Relájate, trata de cubrir lo mejor posible los puntos mencionados,
pero si no logras cumplir con alguno, omítelo y continúa. El objetivo principal de esta prueba es
evaluar tu habilidad para enfrentarte a un desafío.

Relájate, disfruta y a codear!

## Ejemplos de Código


