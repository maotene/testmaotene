import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Test2MainComponent } from './test2-main.component';

describe('Test2MainComponent', () => {
  let component: Test2MainComponent;
  let fixture: ComponentFixture<Test2MainComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Test2MainComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(Test2MainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
