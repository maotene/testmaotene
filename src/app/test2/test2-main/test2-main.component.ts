import { Component, OnInit } from '@angular/core';
import { Book } from './../models/book';
import { BookService } from '../book.service'; // Asegúrate de importar el servicio BookService

@Component({
  selector: 'app-root',
  templateUrl: './test2-main.component.html',
  styleUrls: ['./test2-main.component.scss']
})
export class Test2MainComponent implements OnInit {
  books: Book[] = [];

  constructor(private bookService: BookService) { }

  ngOnInit(): void {
    this.loadBooks();
  }

  loadBooks(): void {
    this.bookService.getBooks()
      .subscribe(books => this.books = books);
  }
}
