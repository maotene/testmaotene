import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesChartComponentComponent } from './sales-chart-component.component';

describe('SalesChartComponentComponent', () => {
  let component: SalesChartComponentComponent;
  let fixture: ComponentFixture<SalesChartComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SalesChartComponentComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SalesChartComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
