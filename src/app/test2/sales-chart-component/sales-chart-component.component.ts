import { Component, OnInit } from '@angular/core';
import Chart from 'chart.js/auto'

import { BookService } from '../book.service';

@Component({
  selector: 'app-sales-chart',
  templateUrl: './sales-chart-component.component.html',
  styleUrls: ['./sales-chart-component.component.scss']
})
export class SalesChartComponent implements OnInit {
  salesData: any; // Supongamos que tienes un array de ventas por libro

  constructor(private bookService: BookService,) { }

  ngOnInit(): void {
    this.getBooks();

  }

  getBooks(): void {
    this.bookService.getBooks()
      .subscribe(books => {
        console.log('maooooo',books)
        this.salesData = books;
      });
  }
  ngAfterViewInit(): void {
    this.generateChart();
  }

  generateChart(): void {
    if (this.salesData) {
      console.log('maooooo teneeee',this.salesData)
      const ctx = document.getElementById('salesChart') as HTMLCanvasElement;
      console.log('maooooo 2222222',ctx)

      new Chart(ctx, {
        type: 'bar',
        data: {
          labels: this.salesData.map(book => book.title),
          datasets: [{
            label: 'Ventas',
            data: this.salesData.map(book => book.sales),
            backgroundColor: 'rgba(54, 162, 235, 0.5)', // Color de fondo de las barras
            borderColor: 'rgba(54, 162, 235, 1)', // Color del borde de las barras
            borderWidth: 1
          }]
        },
        options: {
          scales: {
            
          }
        }
      });
    }
  }
}
