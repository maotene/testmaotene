// book-list.component.ts
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Book } from './../models/book'
import { BookService } from '../book.service';
import { BookDetailsComponent } from '../book-details/book-details.component';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.scss']
})
export class BookListComponent implements OnInit {
  books: Book[] = [];
  topThreeBooks: Book[] = [];

  constructor(
    private bookService: BookService,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.getBooks();
  }

  getBooks(): void {
    this.bookService.getBooks()
      .subscribe(books => {
        this.books = books;
        // Ordenar los libros por número de ventas de forma descendente
        this.books.sort((a, b) => b.sales - a.sales);
        // Tomar los tres primeros libros
        this.topThreeBooks = this.books.slice(0, 3);
      });
  }

  openDialog(book: Book): void {
    this.dialog.open(BookDetailsComponent, {
      width: '400px',
      data: book
    });
  }
}
