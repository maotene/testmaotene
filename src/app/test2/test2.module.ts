import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TestComponent } from './test2/test.component';
import { Test2RoutingModule } from './test2-routing.module';
import { BookListComponent } from './book-list/book-list.component';
import { BookDetailsComponent } from './book-details/book-details.component';
import { Test2MainComponent } from './test2-main/test2-main.component'; // Importa el módulo de enrutamiento
import { MatDialogModule } from '@angular/material/dialog'; // Importa MatDialogModule
import { MatButtonModule } from '@angular/material/button'; 
import { MatTableModule } from '@angular/material/table'; 
import { MatCardModule } from '@angular/material/card'; 
import { MatIconModule } from '@angular/material/icon';
import { MatPaginatorModule } from '@angular/material/paginator';

import { MatGridListModule } from '@angular/material/grid-list';
import { AllBooksComponent } from './all-books/all-books.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { SalesChartComponent } from './sales-chart-component/sales-chart-component.component';
@NgModule({
  declarations: [
    TestComponent,
    BookListComponent,
    BookDetailsComponent,
    Test2MainComponent,
    AllBooksComponent,
    SalesChartComponent
  ],
  imports: [
    CommonModule,
    Test2RoutingModule, // Importa el módulo de enrutamiento,
    MatDialogModule,
    MatTableModule,
    MatCardModule,MatButtonModule,
    MatIconModule,
    MatGridListModule,
    MatPaginatorModule,
    MatFormFieldModule
    ]
})
export class Test2Module { }
