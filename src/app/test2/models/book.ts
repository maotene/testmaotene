// book.ts

// Definición de la clase Book (solo para propósitos de importación)
export class Book {
    title: string;
    author: string;
    publicationDate: Date;
    pages: number;
    isbn: string;
    stock: number;
    sales:number;
  
    constructor(title: string, author: string, publicationDate: Date, pages: number, isbn: string, stock: number,sales:number) {
      this.title = title;
      this.author = author;
      this.publicationDate = publicationDate;
      this.pages = pages;
      this.isbn = isbn;
      this.stock = stock;
      this.sales=sales;
    }
  }
  