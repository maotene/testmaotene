import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { BookService } from '../book.service';
import { Book } from './../models/book';
import { BookDetailsComponent } from '../book-details/book-details.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-all-books',
  templateUrl: './all-books.component.html',
  styleUrls: ['./all-books.component.scss']
})
export class AllBooksComponent implements OnInit, AfterViewInit {
  displayedColumns: string[] = ['title', 'author', 'publicationDate', 'pages', 'isbn', 'stock','sales','actions'];
  dataSource: MatTableDataSource<Book>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  selectedRow: any;

  constructor(private bookService: BookService,
    private dialog: MatDialog
    ) { }

  ngOnInit(): void {
    this.getBooks();
  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
  }

  getBooks(): void {
    this.bookService.getBooks()
      .subscribe(books => {
        console.log('maooooo',books)
        this.dataSource = new MatTableDataSource(books);
      });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  handleRowClick(row:any,index: any): void {
    console.log('maoooooo',row)
    this.selectedRow = row;

  }
  openDetailsDialog(book: Book): void {
    this.dialog.open(BookDetailsComponent, {
      width: '400px',
      data: book
    });
  }
}
