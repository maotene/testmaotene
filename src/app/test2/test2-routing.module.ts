import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TestComponent } from './test2/test.component'; // Importa el componente de prueba si lo has creado
import { Test2MainComponent } from './test2-main/test2-main.component';

const routes: Routes = [
  { path: 'test2', component: Test2MainComponent } // Define una ruta para el componente TestComponent
  // Puedes agregar más rutas aquí si es necesario
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Test2RoutingModule { }
