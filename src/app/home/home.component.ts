import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  ngOnInit(): void {
  }
  redirectToTest(test:number) {
   let url=test==1?'test/test':'test2/test2';
    window.location.href = url;
  }
  
  
}
