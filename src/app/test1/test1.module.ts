import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TestComponent } from './test/test.component';
import { Test1RoutingModule } from './test1-routing.module'; // Importa el módulo de enrutamiento



@NgModule({
  declarations: [
    TestComponent
  ],
  imports: [
    CommonModule,
    Test1RoutingModule // Importa el módulo de enrutamiento
    ]
})
export class Test1Module { }
