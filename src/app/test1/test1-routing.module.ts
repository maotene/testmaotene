import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TestComponent } from './test/test.component'; // Importa el componente de prueba si lo has creado

const routes: Routes = [
  
  { path: 'test', component: TestComponent } // Define una ruta para el componente TestComponent
  // Puedes agregar más rutas aquí si es necesario
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Test1RoutingModule { }
